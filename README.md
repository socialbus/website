### Purpose

This site is made for users to create, administrate and join groups for Socialbus.

Users have to be able to have access to those features without making an account. 

Socialbus' repository  can be found [here](https://gitlab.inria.fr/ldiler/socialbus).

### Setup

the site uses MERN(MongoDB, Express, React, Node) workflow: 

. [MongoDB](https://www.mongodb.com/) to connect and manage the database.

. [Express](https://expressjs.com/fr/) for communication beetween pages, socialbus, and the database.  
Downloadable with npm install express --save in an npm terminal.

. [React](https://fr.reactjs.org/) for the front.

. [NodeJS](https://nodejs.org/en/download/https://nodejs.org/en/download/) for the back.


By default, the app runs on localhost on the port 3000.  
This can be changed in package.json in the  start field.

for example : 
```javascriptstart": "PORT=2000 react-scripts start",```



All the Express communication is made through back/routing.js. It is important to run it as a seperate instance.
By default, it is run on the port 3001, this can be changed by replacing the port variable. 

```javascriptconst port = 2001```

The port used for express has to be  different than the one the website is using.
  
you can start routing.js by going in its directory through command line and using the command:

```npm start routing.js```

### Stucture

app.js is the default page of the site. It uses routes to redirect to other pages 
of the sites which are react class components located in src/components. 

The front end communicates with the back directly through function calls that are located in the page's class.


All the pages have a menu on the top to acces each main page.  

#####Create group page
This page lets the user create a group.  
They will be able to configure speicific fo the groups.   
For now they can only specify a name for the group.

The site then generates a key for the group that can be shared to other future members to join the group.

#####Join group page
This page lets the user join a group by putting in the key of the group shared by people from the group.

If the key entered is value it redirects to the connect page

#####Connect page
This page is for the user to connect with all its medias to Socialbus 
(currently Messenger and slack).  

The user is then either identified by its medias or created if it doesn't exist 
and stored in the database. 

The bot then initiates a conversation with the user in the medias in question.

#####Group configuration page
_not implemented yet_


### Contact

if you need any help or have any issue with the website, you can contact me on my inria email: 
**lior.diler@inria.fr**


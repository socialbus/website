The purpose of this documentation is to understand how to develop for Socialbus. It is only aimed for developers. 

Access tutorials for documentation such as [functional](https://socialbus.gitlabpages.inria.fr/website/tutorial-functionnalDocumentation.html) and [structural](https://socialbus.gitlabpages.inria.fr/webite/tutorial-structuralDocumentation.html) documentation.

Access the modules for the code's detailed documentation.

The documentation for the webhook-manager can be accessed [here](https://socialbus.gitlabpages.inria.fr/webhook-manager).

## 1. Implementing an OSNS <a name="implementing"></a>

In the website, while creating or joining a group, users need to be able to connect to the implemented OSNS with a button.

Conventionnally, OSNS react components are stored in the directory:

Website 🠮 src 🠮 components 🠮 mediaComponents

### 1.1 Creating the button base

First, create a file convetionnally called [OSNS]Button.js.
Socialbus uses [Material UI](https://mui.com/material-ui/getting-started/installation/) for front end components. In this example, we will use the button base of Material UI.

```jsx=
import React, {useState, useEffect} from "react";
import ButtonBase from "@mui/material/ButtonBase";

export default function OSNSButton()
{
	return (
		<ButtonBase>
                    Log in with [OSNS]
		</ButtonBase>
	)
}
```

### 1.2 Adding a dynamic name and state

The state of the button needs to be variable based on if the user has logged in or not.

Conventionnally, the default content of the text is "log in with [OSNS]" which changes to "logged in" and the button becomes disabled.

We can use React states for this effect.

```jsx=
import React, {useState, useEffect} from "react";
import ButtonBase from "@mui/material/ButtonBase";

export default function OSNSButton()
{
    //creating states with default values
    const [buttonText, setButtonText] = useState("Log in with OSNS");
    const [disabled, setDisabled] = useState(false);
    
    return (
        <ButtonBase disabled={disabled}>
                    {buttonText}
        </ButtonBase>
    )
}
```

### 1.3 Adding a click callback

Upon clicking on the button, the user will be prompted with the OSNS' procedure to authorize Socialbus to communicate with the user. At the same time, Socialbus stores the ID of the user in the OSNS in the back-end of the site. 

These procedures are specific to each OSNS and cannot be represented generically.

```jsx= 
import React, {useState, useEffect} from "react";
import ButtonBase from "@mui/material/ButtonBase";

export default function OSNSButton()
{
    //creating states with default values
    const [buttonText, setButtonText] = useState("Log in with OSNS");
    const [disabled, setDisabled] = useState(false);
	
    function onButtonClick()
    {
        //OSNS' procedure is implemented here.
    }

    return (
        <ButtonBase onClick={() => onSlackButtonClick() disabled={disabled}>
                    {buttonText}
        </ButtonBase>
    )
}
```

### 1.4 On focus logged in verification

Some OSNS' will inform the client side entity of the website of the user's ID while some others won't. In the case where the ID is only stored in the back, the website still needs to show the user that they have succesfully logged in.

This is when we can do a database verification of if the user's ID is stored.

First we need to listen the "focus" callback which is called when the user returns to the website's tab. To force the user to focus on the tab at least once, the OSNS' procedure is opened on another tab and the focus is set to it.

The database user is created when the user navigates to the create or join page. This can be transmitted through an argument to the component

```jsx= 
import React, {useState, useEffect} from "react";
import ButtonBase from "@mui/material/ButtonBase";
//import the post api to request to the database
import {post} from "../../services/api/Provider";

//Get the user's ID through the parent component
export default function OSNSButton({databaseUserID})
{
    //creating states with default values
    const [buttonText, setButtonText] = useState("Log in with OSNS");
    const [disabled, setDisabled] = useState(false);
	
    function onButtonClick()
	{
        //remove the previous "focus" listener if existant
        window.removeEventListener("focus", onFocus);
        //create a new listener on the "focus" callback
		window.addEventListener("focus", onFocus);
        
	    //OSNS' procedure opened at a given url
        window.open([url], '_blank').focus();
	}
    
    function onFocus()
    {
        const body = {
            userID:userID
        }
        
        post("/getUserMedias", body).then(medias => {
        //if the user's medias contains the OSNS, set the button as logged in
        if (medias.find(media => media.media === "OSNS") !== undefined) {
            setDisabled(true);
            setButtonText("Logged in");
        }
    });

    }

    return (
        <ButtonBase onClick={() => onSlackButtonClick() disabled={disabled}>
                    {buttonText}
        </ButtonBase>
    )
}
```

### 1.5 Adding the button to the Connect component

Finally, the button needs to be added to the [Connect component](https://socialbus.gitlabpages.inria.fr/website/module-Connect.html). The [Connect component](https://socialbus.gitlabpages.inria.fr/website/module-Connect.html) is the button layout used in the Create and Join page for users to log in to the OSNS of their choice.

The button needs to be added as a grid item. This will make the button automatically aligned with the other OSNS buttons.

```jsx=
<Grid container rowSpacing={8} columnSpacing={6} style={{textAlign: "center"}}>
        <Grid item xs={4}>
            <OSNSButton userID={userID}/>
        </Grid>
</Grid>
```



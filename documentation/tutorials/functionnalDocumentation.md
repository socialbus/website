Socialbus’ website is made for Socialbus users to create, configure, and join groups to use in OSNS. 
No account is needed within Socialbus as OSNS are used to authenticate users instead. 
Groups are identified with keys and users are identified by the OSNS used to connect to a group. 

This documentation goes through each page of the website more in detail. To see a use case of socialbus, refer to [according page](https://socialbus.gitlabpages.inria.fr/webhook-manager/tutorial-functionnalDocumentation.html).

## Summary

### - 1. [Home page](#home)

### - 2. [Create page](#create)

### - 3. [Join page](#join)

### - 4. [Log in with Slack](#login_slack)

### - 5. [Log in with Messenger](#login_messenger)

### - 6. [Log in with Twitter](#login_twitter)

## 1. Home page <a name="home"></a>

This is where users will start browsing the website.

They can navigate through the other important pages here.

![](https://notes.inria.fr/uploads/upload_f74f69806d41bf6566e0ebd549582036.png)

*Figure 1: Home page*

Upon changing pages, the menu moves to the top and the page is displayed. 

## 2. Create page <a name="create"></a>


![](https://notes.inria.fr/uploads/upload_0297c89ecaba7aa80a025d90d7b81f66.png)

*Figure 2: Create page*

To create a group, the user must be provid several information:

- The name of the group
- The nickname they will use for the group
- The OSNS they wish to connect the group to.


Once the parameters are filled in, the user can click on the submit button.

If all mandatory information is filled in, the group will be created in the database and a key will be displayed. Otherwhise, a message will display showing what information is incorrect.
The user can invite other users to join their group with that key.

In the figure 3 below, the user doesn't connect with an OSNS. 

![](https://notes.inria.fr/uploads/upload_7f904682a78a22c8531ba33547063287.png)

*Figure 3: A user forgetting to connect with an OSNS*

The figure 4 below shows a user successfully creating ag roup.

![](https://notes.inria.fr/uploads/upload_6f4fbe57e087660239078aadfeab3a63.png) 

*Figure 4: A user successfully creating a group connecting with twitter*

## 3. Join group page <a name="join"></a>

![](https://notes.inria.fr/uploads/upload_755aa38ced09c15c6c2e1f0ca904f7a8.png)

*Figure 5: The join page*

To join a group, the creator of the group must inform the users of the group's key. The user must then inform the nickname for the group along with the OSNS they wish to connect with.

If they successfully join the group, they will be prompted with a message informing them. Otherwhise, they will be prompted with the message of what information is missing.

![](https://notes.inria.fr/uploads/upload_57d571a222002596063f53db6603e33a.png)

*Figure 6: A user successfully joining a group* 

## 4. Log in with Slack <a name="login_slack"></a>

While creating or joining a group, the user can log in with Slack. 
This will open another tab where they can press a button to allow the use of Socialbus in slack. If the procedure is succesfful, the button grays out and writes "Logged in".

![](https://notes.inria.fr/uploads/upload_2c55d38efb49bdd35f3de948484ceb63.png)

*Figure 7: A user gives Sociablus permissiion to communiquate with them.* 

## 5. Log in with Messenger <a name="login_messenger"></a>

While creating or joining a group, the user can log in with Messenger.
This will open another tab opening a conversation with the Socialbus bot in messenger. The button grays out and writes "Logged in".

## 6. Log in with Twitter <a name="login_twitter"></a>


While creating or joining a group, the user can log in with Twitter.
This will open a window where they get an explanation of how to log in with twitter.
![](https://notes.inria.fr/uploads/upload_b96fcf485c71442ac84ee30514fc4789.png)
*Figure 8: Twitter window upon click on the log in with Twitter button*

The user needs to press on the "Twitter" button  which will prompt them to log in.

This process provides the user with a pin code they have to give to the website in a textfield. Based on if the pin is valide or not, a promp will appear displaying the outcome.

![](https://notes.inria.fr/uploads/upload_64a141feb9675088630508d08d29f21f.png)
*Figure 9: A user logging in with Twitter.*


## Configure group

This page has not yet been implemented.
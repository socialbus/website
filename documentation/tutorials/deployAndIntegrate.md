## Summary

### - 1. [Introduction](#introduction)

### - 2. [Setting up the repository](#setting_up)

### - 3. [Docker](#docker)

## 1. Introduction <a name="introduction"></a>

To understand the general functionality of Socialbus please refer to the [functional documentation](https://socialbus.gitlabpages.inria.fr/webhook-manager/tutorial-functionnalDocumentation.html). The purpose of this documentation is to understand how to set up and deploy Socialbus and develop on top of the existing application. 


## 2. Setting up the repository <a name="setting_up"></a>

Socialbus has a repository group that can be found in [inria's gitlab](https://gitlab.inria.fr/socialbus).

There are 4 repositories:

- Documentation: PDFs of the documentation for socialbus.
- Platform: The main repository. It handles docker-compose and uses other repositories as dependencies
- Webhook-manager: Contains the code and dockerfiles for OSNS' and message sending
- Website: Has the front and the back along with the dockerfiles to build them. 

### 2.1 Cloning the repository.

Pulling the platform repository is all that is required. the rest of the contents of the application will be pulled through git submodules.

    git clone https://gitlab.inria.fr/socialbus/Platform.git
    
### 2.2 Git submodules

Once the repository is cloned, we need to use a command to clone the dependencies which is the rest of the application. 


Use the command below to clone the submodules:

    git submodule update --init --recursive

### 2.3 Setting up the environnement variables

The docker compose uses an .env file. It is used to easily configure how the containers should be configured when ran.
The format needs to be as the one below:

```
#PORTS
MESSENGERPORT=""
TWITTERPORT=""
SLACKPORT=""
NGINXPORT=""
EXPRESSPORT=""
WEBSITEPORT=""
MONGO_PORT=""

#HOSTNAMES AND PATHS
PROTOCOL=https <-- this can be changed to http for local tests
HOSTNAME="socialbus.paris.inria.fr" <-- has to be changed if hosted in a different domain
MONGO_HOST="mongoDB" <-- name of the mongoDB container
RABBITMQ_HOST="rabbitMQ" <-- name of the rabbitMQ container

#WEBSITE ENV VARS
REACT_APP_EXPRESS_PATH="socialbus.paris.inria.fr/express" <-- Where the website should send GEt and POST requests.
REACT_APP_PROTOCOL=https <-- this can be changed to http for local tests
WEBSITEVOLUME="/usr/share/nginx/react" <-- Where the website is located for deployement

#MESSENGER
MESSENGER_VERIFY_TOKEN="" 
MESSENGER_BOT_ID=""
MESSENGER_BOT_TOKEN=""

#TWITTER
TWITTER_CONSUMER_KEY=""
TWITTER_CONSUMER_SECRET=""
TWITTER_TOKEN=""
TWITTER_TOKEN_SECRET=""
TWITTER_BOT_ID=""
TWITTER_APP_NAME=""

#SLACK
SLACK_CLIENT_ID=""
SLACK_CLIENT_SECRET=""
SLACK_BOT_TOKEN=""
SLACK_VERIFY_TOKEN=""
```

### 2.4 Deploying the application

To function, Socialbus needs to be accessible through an https url which implies that a trusted SSL certificate is used.
It is recommended to use NGINX to deploy Socialbus, but any other technology can be used.

#### Website paths

- Default path: redirects to the website

#### OSNS paths

- Each OSNS must have its own path which redirects to the docker container of the targeted OSNS at the /webhook location of the given port.

The format of a redirection is as below:
http://[containerName]:[containerPort]/webhook.

#### Documentation paths

- Webhook Manager: the "/webhook-manager/documentation" path redirects to the documentation for the webhook manager.
- Website: the "/website/documentation" path redirects to the documentation for the website.

### 2.5 Building and running Socialbus

[Docker](https://docs.docker.com/get-docker/) and [Docker Compose](https://docs.docker.com/compose/install/) need to be downloaded to be able to run the application.

Once all these steps are completed,the application is ready to be built and ran. 

The command below builds the docker containers:

    docker-compose build

The command below runs the application:

    docker-compose up
    
Both can be done in the same command:

    docker-compose up --build


## 3. Docker <a name="docker"></a>

Socialbus has been dockerized to be able to deploy Socialbus on any machine. It also makes various components of Socialbus independent from one another.

### 3.1 Containers

Each autonomous part of Socialbus has been divided into smaller docker containers:

-	One for each OSNS (currently Messenger, Twitter, Slack)
-	The database
-	The reverse proxy
-	RabbitMQ
-	Message receiver
-	building the frontend of the website
-	building the documentation of Socialbus
-	The backend of the website
-	A docker-compose allows you to orchestrate and launch everything smoothly.

### 3.2 Containers diagram

The diagram below represents the docker containers used in Socialbus.

![](https://notes.inria.fr/uploads/upload_e0df465bb32778140f209b7233f944ee.png)
*Figure 1: Diagram of Socialbus' docker containers* <a name="fig1"></a>

### 3.3 Container flow

#### Docker-compose

Orchestrates the launching of Socialbus. It manages the launch of docker containers, the order in which they are launched, the environment variables, and the ports allocated to each container.

#### Webhook Manager

##### OSNS' webhooks

Each OSNS has its own instance of a webhook container. To have more information on what they do, refer to the [structural documentation](https://socialbus.gitlabpages.inria.fr/webhook-manager/tutorial-structuralDocumentation.html).

##### Receiver

The receiver container listens to RabbitMQ queues receiving data an OSNS’ webhook. It then prepares the message for the targeted OSNS' and sends the messages in them.

##### Documentation

This container makes a build of the documentation for the webhook-manager and stores it in a shared volume of the main machine. That shared volume is what the reverse proxy redirects to. The container then stops itself.
Statically generates the documentation for the webhook manager.

#### General

Containers categorized in general are used by other containers to function or to communicate between each other.

##### Database

Container that stores all the data from Socialbus and its website with a mongoDB database. For instance, it stores the user groups created by the website, the users that joined them, etc.

##### RabbitMQ

Implements a queuing system for OSNS’ to communicate data to the receiver container.

##### Reverse-proxy

NGINX Web server that allows users to access the website, documentation and OSNS’ to communicate with Socialbus.

#### Website 

##### React

This container makes a production build of the website and stores it in a shared volume of the main machine. That shared volume is what the reverse proxy redirects to. The container then stops itself.

##### Express

Receives Get and Post requests from the website and communicates with the database to do various tasks with users and groups such as creating groups, joining a group, etc.

##### Documentation

This container makes a build of the documentation for the webhook-manager and stores it in a shared volume of the main machine. That shared volume is what the reverse proxy redirects to. The container then stops itself.
Statically generates the documentation for the webhook manager.

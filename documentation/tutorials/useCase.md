---
tags: mimove, socialbus, webhook-manager, website
---

# Socialbus use case

Ben is in a discussion group with his friends David and Josh, which like using Messenger. He is also in a reading club with David, but with also Gamran and Natalie which prefer using Twitter. Finally, he is a member of a Slack group for work.  
This is why Ben uses Socialbus so that he can communicate in these three groups only with his prefered social media.

In order to communicate with his friends David and Josh, Ben goes to Socialbus' website to create a discussion group.


![homepage](https://notes.inria.fr/uploads/upload_9c5d0594cc1778436bad63aa70b51a7d.png)

*Socialbus' home page*

He clicks on the create group button.


![createpage](https://notes.inria.fr/uploads/upload_a4e205076c5c47eb19660b51ec07e3e1.png)

*Socialbus' create group page*

He can now start filling the information to create the group.
He fills in the name of his group, his nickname in the group and connects the social medias he wishes to use for Socialbus, in his case Twitter.

![](https://notes.inria.fr/uploads/upload_b2a17143a7d6fac534ab8886a2b2b908.png)

*Ben filled the information for the group*

Once all the information filled in, he can press on the button "create group" to get a group key he can send to David and Josh for them to join the group.

![](https://notes.inria.fr/uploads/upload_6f4fbe57e087660239078aadfeab3a63.png) 

*Ben successfully created the friend group*

David and Josh can now join the group through the website by pressing the "Join Group" button in the website.
They have to enter the group key Ben gave to them along with their nickname in the group and the social medias they wish to use with Socialbus, in their case Messenger.

![](https://notes.inria.fr/uploads/upload_8eaba3396606d93e182bdb82bc1db4c4.png)

*David Enters his information to join the group*




He then clicks on the "Join Group" button to finalize the process.
![](https://notes.inria.fr/uploads/upload_57d571a222002596063f53db6603e33a.png)

*David joins the friend group* 

Now that he has joined the group, he can start using Socialbus on his favorite social media.

----------
Since it is the first time David and Josh use Socialbus, they can use the "Help" command to see all the available commands.

![](https://notes.inria.fr/uploads/upload_33583c093eab745ad2eb49be9f24c376.png)

*David  types help to learn about available commands*

He can use the "connectToGroup" command to start receiving messages from his friend group. If he doesn't follow it by the name of the group, he will be prompted with all the groups he is connected to.

Upon connecting, he will get the latest messages sent in the group.


![](https://notes.inria.fr/uploads/upload_9894700be2c0de20e0a996a5fbad7ebe.png)

*David connects to the friend group*

David now receives messages from other members of the group.

![](https://notes.inria.fr/uploads/upload_fc77a04e8362d1da1d3cd8787ea0c724.png)

*David automatically receives messages in real time*

To start replying to his friends, David uses the command "openSession" and chooses the friend group. Every message that he sends is now directly sent to everyone connected in the group.

![](https://notes.inria.fr/uploads/upload_709b2b3569d10aea84d05bdea4c21fd2.png)

*David replies to the messages with the open session command.*

Other members of the group receive the message despite using another social media. In the meantime, Ben is receiving messages from the reading club he is part of. 
He can differentiate which group he is receiving messages thanks to the tag next to their nickname.

![](https://notes.inria.fr/uploads/upload_d06912cba5d67b71febe6b9f15a7689e.png) 

*Ben gets messages from multiple groups*

In order to reply to them, he closes the session with the my friends group to open one with the reading club.

![](https://notes.inria.fr/uploads/upload_16db0beed6e08913d56f6f78a15c513c.png)

*Ben changes his session to reply to the reading club.*

Since ben uses multiple groups, receiving messages from mutliple groups can be overwhelming. He can use the disconnectFromGroup command to stop receiving messages in real time.
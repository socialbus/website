import {post} from "./api/Provider";


export default function NotifyMembersOfJoin(medias, userID, groupID, groupName, nickname, action)
{
    NotifyGroupMembers(medias, userID, groupID, groupName, nickname);
    NotifyGroupJoiner(medias, userID, groupID, groupName, nickname, action);
}

//The users of the group are notified of a member joining
function NotifyGroupMembers(medias, userID, groupID, groupName, nickname)
{
    const userData = {
        connectors:{
            user: medias[0].id,
	    id:userID
        },
        targetGroup: groupID,
        isJoinMessage: true,
        message: nickname + " just joined the group " + groupName + "."
    }
    post("/NotifyGroupMembersOfJoin", userData);
}
//The user that joined receives a welcome message
export function NotifyGroupJoiner(medias, userID, groupID, groupName, nickname, action)
{
    const userData = {
        connectors:{
            user: medias[0].id,
	    id:userID
        },
        isJoinMessage: true,
        message: "Welcome to Socialbus " + nickname + "! You just " + action + " the group " + groupName + ".\nto get started, use commands. See available commands by typing !help"
    }
    post("/NotifyGroupMembersOfJoin", userData);
}

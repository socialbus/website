import {handleResponse, handleError} from "./Response";

require('dotenv').config({path:"../.env"})

const API_URL = `${process.env.REACT_APP_PROTOCOL}://${process.env.REACT_APP_EXPRESS_PATH}`;

const post = async (path, body) => {
	return await fetch(`${API_URL}${path}`, {
		method: "POST",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			json: true
		},
		body: JSON.stringify(body)
	}).then(handleResponse)
		.catch(handleError);
}

const get = async (path, body) => {
	return await fetch(`${API_URL}${path}`, {
		method: "GET",
		headers: {
			"Accept": "application/json",
			"Content-Type": "application/json",
			json: true
		},
		body: JSON.stringify(body)
	}).then(handleResponse)
		.catch(handleError);
}
export {post, get};
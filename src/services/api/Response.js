export function handleResponse(response)
{
	if(!response.ok)
		throw new Error();
	if(response.json)
		return response.json();
	if(response.data)
		return response.data;
	if(response.results)
		return response.results;
	if(response)
		return response;
}

export function handleError(error)
{
	console.log("error:", error);
	if(error.data){
		return error.data;
	}
	return error;
}
import React from "react";
import {Typography} from "@mui/material";

function ConditionalTypography({content, active, sizeVariant, cssStyle}) {

	if(active)
		return (
			<Typography variant={sizeVariant} style={cssStyle} children={content}/>
		)
	else
		return null;
}

export default ConditionalTypography;

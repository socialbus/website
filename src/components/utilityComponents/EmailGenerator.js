/**
 * @module EmailGenerator
 * @category Utility
 * @description Generates an email with a link to invite other people in the newly created group.
 */
import React from "react";
import {Link} from "@mui/material";


require('dotenv').config({path:"../.env"})

/**
 *
 * Link to a generated email for a group that was created. Clicking on the link makes the user appear on the join page with the group key textfield automatically filed in.
 * @param {string} groupName - Name of the group that was created
 * @param {string} groupKey - Key of the group used to join the group.
 * @param {boolean} displayEmail - If false, the link isn't displayed.
 * @returns {HTMLElement|null} The html that displays the generated link
 */
export default function EmailGenerator ({groupName, groupKey, displayEmail}) {

	const contentFirstPart = "mailto:?subject=Invitation to Socialbus group"
	const contentSecondPart = ".&body=Hi%2C%0D%0AI%20created%20a%20Socialbus%20group%20called%20"
	const contentThirdPart = ".%0D%0AClick%20the%20link%20below%20to%20join%20the%20group!%0D%0A"

	let url = process.env.REACT_APP_PROTOCOL + "://" + process.env.REACT_APP_HOSTNAME + "/join?groupKey=" + groupKey
	if(displayEmail)
	{
		return (
			<div id="email link div"
			style={{
				"text-align": "center",
			}}>
				<Link href={contentFirstPart + groupName + contentSecondPart + groupName + contentThirdPart + url}>Or invite other people by mail</Link>
			</div>
		)
	}
	else
		return null;
}


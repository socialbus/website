/**
 * @module ErrorDisplayer
 * @category Utility
 * @description Message displayed in red if an information entered by the user is missing or incorrect.
 */
import React from "react";
import {Typography} from "@mui/material";

/**
 *
 * @param {string} errorMessage - The error message to display
 * @param {boolean} displayError - If false, the message isn't displayed.
 * @returns {null|HTMLElement}
 */
export default function ErrorDisplayer({errorMessage, displayError})
{
	if(displayError)
	{
		return(
			<Typography variant="h5" align="center" color="red">{errorMessage}</Typography>
		)
	}
	else
		return null;
}
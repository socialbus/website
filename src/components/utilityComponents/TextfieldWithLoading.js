/**
 * @module TextfieldWithLoading
 * @category Utility
 * @description Textfield containing an animated loading icon aligned next to it. When an action is completed in the parent component, displays a message instead.
 */
import React from "react";
import TextField from "@mui/material/TextField";
import CircularProgress from '@mui/material/CircularProgress';
import {Typography} from "@mui/material";
import EmailGenerator from "./EmailGenerator";
/**
 *
 * @param {boolean} created - If true, a message is displayed instead of the textfield.
 * @param {boolean} loading - If true and created is false, a loading icon is displayed.
 * @param {function} setTextFieldValue - The react hook state that modifies a textfield value in the parent component. This is set when the user types in the textfield.
 * @param {string} groupID - The group ID to display.
 * @param {string} mainMessage - The main message to display instead of the textfield.
 * @param {string} subMessage - The sub message to display instead of the textfield.
 * @param {string} label - The label of the textfield.
 * @returns {HTMLElement}
 */
export default function TextfieldWithLoading({created, loading, textFieldValue, setTextFieldValue, groupID, mainMessage, subMessage, label, displayEmail}){

	if(created)
		return(
			<div>
				<Typography variant="h5" align="center" color="green">{mainMessage}</Typography>
				<Typography variant="h6" align="center">{subMessage}</Typography>
				<Typography variant="h6" align="center">{groupID}</Typography>
				<EmailGenerator groupName={textFieldValue} groupKey={groupID} displayEmail={displayEmail}/>
			</div>
		)
	if(!loading){
		return (
			<TextField id={label}  label={label} variant="outlined" value={textFieldValue} onChange={(event
			) => setTextFieldValue(event.target.value)} />
		)
	}

	return(
		<CircularProgress />
		)

}

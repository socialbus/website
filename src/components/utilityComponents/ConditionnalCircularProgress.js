/**
 * @module ConditionnalCircularProgress
 * @category Utility
 * @description A loading animation while waiting for a process to be done.
 */
import React from "react";
import Box from "@mui/material/Box";
import CircularProgress from "@mui/material/CircularProgress";

/**
 *
 * @param {boolean} isLoading - If false, nothing is displayed.
 * @returns {HTMLElement|null}
 */
export default function ConditionnalCircularProgress({isLoading})
{

	if(isLoading)
	{
		return(
		<div>
			<Box display="flex" alignItems="center"	justifyContent="center">
				<CircularProgress size={30} />
			</Box>
		</div>
		)
	}
	else
		return null;

}
/**
 * @module BackButtonContainer
 * @category Utility
 * @description The button to return to the home page. This is only displayed when not in the default page.
 */
import Fab from "@mui/material/Fab";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import React from "react";
import {useLocation, useNavigate} from "react-router-dom";
import {showStyle, hiddenStyle} from "../AnimationStyles"

/**
 *
 * @param {string} location - The current location of the user.
 * @returns {boolean} returns true if at the default location.
 */
function isDefaultLocation(location){
	console.log("location is :", location.pathname);
	return location.pathname !== "/";
}

/**
 *
 * @param {function} setNavBarSize - The react hook state that modifies the navBarSize in a parent component.
 * @param {function} setStyleState - The react hook state that modifies the styleState in a parent component.
 * @returns {HTMLElement|null}
 * @constructor
 */
export function BackButtonContainer({setNavBarSize, setStyleState}){

	const location = useLocation();
	let navigate = useNavigate();

	function handleClick(path)
	{
		setStyleState(hiddenStyle);

		setTimeout(() => {
			setNavBarSize(1);
			navigate(path)
			setTimeout(() => {
				setStyleState(showStyle)
			}, 500);
		}, 500);
	}

	if(!isDefaultLocation(location))
		return null
	else
	{
		return (
			<div style={{
				position: "absolute",
				left: "2em",
				top: "2em"
			}}>
				<Fab color="primary" aria-label="add" onClick={()=> {
					handleClick("/");

				}}>
					<ArrowBackIcon/>
				</Fab>
			</div>)
	}
}

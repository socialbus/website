const showStyle= {
	visibility:"visible",
	opacity: 1,
	transition: "opacity 1.5s linear",
	flex: 1
}

const hiddenStyle = {
	visibility : "hidden",
	opacity : 0,
	transition : "visibility 0s 1.5s, opacity 0.5s linear",
	flex: 1
}

module.exports = {showStyle, hiddenStyle}
/**
 * @module MessengerButton
 * @category MediaComponents
 * @description The log in with Messenger button displayed in the Connect component.
 */
import React, { useEffect, useState} from 'react';
import {post} from "../../services/api/Provider";
import ButtonBase from "@mui/material/ButtonBase";
/**
 *
 * The button redirects to a messenger page opening a conversation with the Socialbus bot with an optin parameter of the user ID in parameter to the Messenger webhook.
 * The Messenger webhook stores the found messenger ID to the user ID in the database.
 * When refocusing on the website's page, this component verifies if the user was successfully stored. If it is, the button is grayed out and displays "Logged in".
 * @param userID - the user ID generated in the database.
 * @returns {HTMLElement}
 */
export default function MessengerButton({userID})
{
	const [buttonText, setButtonText] = useState("Log in with Messenger");
	const [buttonOpacity, setButtonOpacity] = useState(1);
	const [disabled, setDisabled] = useState(false);

	function onFocus(){
		const body = {
			userID:userID
		}
		post("/getUserMedias", body).then(medias => {
			console.log("user medias:", medias);
			if (medias.find(media => media.media === "Messenger") !== undefined) {
				setDisabled(true);
				setButtonText("Logged in");
				setButtonOpacity(0.5);
			}
		});
	}
	function onButtonClick()
	{
		window.removeEventListener("focus", onFocus);
		window.addEventListener("focus", onFocus);
		window.open(`http://m.me/101304151352538?ref=${userID}`, '_blank').focus();
	}

	useEffect(() => {
		return () => {
			window.removeEventListener("focus", onFocus);
		};
	}, [])


	return(
		<ButtonBase
			type="submit"
			color="primary"
			variant="contained"
			disabled={disabled}
			style={{
				"alignItems":"center",
				"color": "#000",
				"backgroundColor":"#fff",
				"border":"1px solid #ddd",
				"borderRadius":"4px",
				"display":"inline-flex",
				"fontFamily":"Lato, sans-serif",
				"fontSize":"16px",
				"fontWeight":"600",
				"height":"48px",
				"justifyContent":"center",
				"textDecoration":"none",
				"width":"200px",
				"textTransform": "none",
				"opacity":buttonOpacity
			}}

			onClick={() => onButtonClick()}>
			<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 48 48" width="24px" height="24px">
					<path  d="M 44 23.5 C 44 34.269531 35.050781 43 24 43 C 22.347656 43 20.75 42.804688 19.214844 42.4375 C 18.75 42.324219 18.265625 42.367188 17.835938 42.582031 L 13.460938 44.769531 C 12.328125 45.335938 11 44.511719 11 43.25 L 11 39.222656 C 11 38.648438 10.742188 38.113281 10.320312 37.726562 C 6.425781 34.164062 4 29.109375 4 23.5 C 4 12.730469 12.949219 4 24 4 C 35.050781 4 44 12.730469 44 23.5 Z M 44 23.5" fill="#00B2FF"/>
					<path d="M 34.394531 18.5 L 28.695312 22.722656 C 28.085938 23.179688 27.253906 23.179688 26.652344 22.730469 L 22.679688 19.738281 C 21 18.488281 18.621094 18.921875 17.488281 20.679688 L 16.28125 22.570312 L 12.171875 29.25 C 11.570312 30.191406 12.71875 31.261719 13.609375 30.589844 L 19.308594 26.371094 C 19.921875 25.910156 20.75 25.910156 21.351562 26.359375 L 25.324219 29.351562 C 27.003906 30.601562 29.382812 30.171875 30.515625 28.410156 L 31.722656 26.519531 L 35.835938 19.839844 C 36.433594 18.902344 35.285156 17.832031 34.394531 18.5 Z M 34.394531 18.5 " fill="#ffffff"/>
			</svg>

			{buttonText}</ButtonBase>
	)
}

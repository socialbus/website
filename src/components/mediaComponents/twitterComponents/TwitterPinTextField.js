/**
 * @module TwitterPinTextField
 * @category MediaComponents
 * @description The textfield in which the user can enter a generated twitter ID. If the pin is valid, the textfield becomes green and prompts the user to close the modal. Otherwise, the user textfield becomes red and displays that the pin is invalid.
 */
import React from "react";
import TextField from "@mui/material/TextField";
import {Stack} from "@mui/material";
import ConditionnalCircularProgress from "../../utilityComponents/ConditionnalCircularProgress";
import {post} from "../../../services/api/Provider";

/**
 *
 * @param {string} twitterOauth - The twitter token received by a request to the backend.
 * @param {string} twitterOauthSecret - The twitter token secret received by a request to the backend.
 * @param {function} updateMedia - The react hook state that updates the stored medias.
 * @param {function} setLoggedIn - The react hook state that updates grays out the Twitter button and changes the text to "Logged in".
 * @returns {HTMLElement}
 */
export default function({twitterOauth, twitterOauthSecret, updateMedia, setLoggedIn})
{

	const [pinError, setPinError] = React.useState(false);
	const [pinErrorMessage, setMessage] = React.useState("");
	const [fieldColor, setFieldColor] = React.useState("");
	const [isValidating, setIsValidating] = React.useState(false);
	/**
	 *
	 * When the textfield is modified, if there are 7 characters verify if the pin is valid.
	 * @param event - The textfield being modified by a user.
	 */
	const handleChange = (event) => {
		if(event.target.value === "")
		{
			setPinError(false);
			setFieldColor("");
			setMessage("");
		}

		if(event.target.value.length === 7)
			handleConfirm(event);

	};

	/**
	 *
	 * Verifies if the pin is valid or not.
	 * @param event
	 */
	function validatePin(event)
	{
		setIsValidating(true);
		//TODO add state verification to this request
		const body = {
			"twitterToken": twitterOauth,
			"twitterTokenSecret":twitterOauthSecret,
			"pin":event.target.value
		}
		post("/twitterValidatePin", body).then(twitterJson=>{
			setIsValidating(false);
			if(twitterJson.status === 204)
			{
				//user subscribed for the first time
				setLoggedIn();
				updateMedia("Twitter", twitterJson.twitterID);
				setFieldColor("success");
				setMessage("The pin was validated. You can close this tab");
			}
			else if(twitterJson.status === 409 || twitterJson.status === 355)
			{
				//user already susbcribed
				setLoggedIn();
				updateMedia("Twitter", twitterJson.twitterID);
				setFieldColor("success");
				setMessage("The pin was validated. You can close this tab");
			}
			else if(twitterJson.status === 401)
			{
				setPinError(true);
				setMessage("Invalid pin, please try again")
			}
			else
			{
				setPinError(true);
				setMessage("Invalid pin, please try again")
			}
		})
	}


	function handleConfirm(event)
	{
			console.log("enter key pressed with value: ", event.target.value);

			if(event.target.value === "")
				return;
			if(isNaN(parseInt(event.target.value)))
			{
				setPinError(true);
				setMessage("pin should be only numbers");
				return;
			}

			validatePin(event);
	}


	return (
		<div>
			<Stack direction="row" spacing={2}>
				<TextField id="Twitter Pin"
						   error={pinError}
						   helperText={pinErrorMessage}
						   label="Twitter Pin"
						   color={fieldColor}
						   variant="outlined"
						   onChange={handleChange}
				/>
				<ConditionnalCircularProgress isLoading={isValidating}/>
			</Stack>
		</div>
	)
}

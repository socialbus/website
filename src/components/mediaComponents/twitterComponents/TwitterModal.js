/**
 * @module TwitterModal
 * @category MediaComponents
 * @description The modal which appears when a user clicks on the log in with Twitter button.
 */
import React from "react";
import Modal from "@mui/material/Modal";
import Box from "@mui/material/Box";
import {Button, Stack, Typography} from "@mui/material";
import TwitterPinTextField from "./TwitterPinTextField";
import {get} from "../../../services/api/Provider";

const style = {
	position: 'absolute',
	top: '50%',
	left: '50%',
	transform: 'translate(-50%, -50%)',
	width: 400,
	bgcolor: 'background.paper',
	border: '2px solid #000',
	boxShadow: 24,
	p: 4,
};

/**
 *
 * @param {boolean} open - Renders the modal if true
 * @param {function} handleClose - The react hook state that modifies the open state in the parent component. Sets the value to false when called.
 * @param {function} updateMedia - The react hook state that updates the stored medias. It is passed down to the children component TwitterPinTextField.
 * @param {function} setLoggedIn - The react hook state that updates grays out the Twitter button and changes the text to "Logged in". It is passed down to the children component TwitterPinTextField.
 * @returns {HTMLElement}
 */
export default function TwitterModal({open, handleClose, updateMedia, setLoggedIn})
{

	const [twitterOauth, setTwitterOauth] = React.useState("");
	const [twitterOauthSecret, setTwitterOauthSecret] = React.useState("");

	function onLoginTwitter()
	{
		get("/twitterAccessToken", undefined).then(twitterCredentialsJson => {
			setTwitterOauth(twitterCredentialsJson.twitterToken);
			setTwitterOauthSecret(twitterCredentialsJson.twitterSecretToken);
			window.open(twitterCredentialsJson.url,'_blank');
		})
	}
	return (
		<Modal
			open={open}
			onClose={handleClose}
		>
			<Box
				sx={style}
			>
				<Stack direction="column" spacing={6}>
					<Typography>1. Click on the twitter button to login wit the account to associate.</Typography>
					<Button
						onClick={() => onLoginTwitter()}
						type="submit"
						color="primary"
						variant="contained"
					>Twitter</Button>
					<Typography>2. Enter the pin given from logging in in the field below</Typography>
					<Box display="flex" alignItems="center"	justifyContent="center">
						<TwitterPinTextField twitterOauth={twitterOauth} twitterOauthSecret={twitterOauthSecret} updateMedia = {updateMedia} setLoggedIn={setLoggedIn}/>
					</Box>
				</Stack>
			</Box>
		</Modal>
	)
}

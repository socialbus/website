/**
 * @module Connect
 * @category Core
 * @description Handles the layout of buttons to log in to all the OSNS' along with creating a new user in the database. This is used in the create and join page.
 */
import React, {useState, useEffect} from "react";
import {Button} from "@mui/material";
import Grid from "@mui/material/Grid";
import TwitterButton from "../mediaComponents/twitterComponents/TwitterButton";
import MessengerButton from "../mediaComponents/MessengerButton";
import {post} from "../../services/api/Provider";
import SlackButton from "../mediaComponents/SlackButton";

/**
 *
 * The buttons are in a responsive grid, automatically aligning the buttons.
 * @param {function} updateOrAddMedia - the parent component function to add a newly connected media.
 * @param {string} userID - The created user ID in the database. By default it is blank but is set when the Connect component is loaded.
 * @param {function} setUserID - The react hook state that modifies the userID parameter. It is used in the parent component.
 * @returns {HTMLElement}
 */
function Connect({updateOrAddMedia, userID, setUserID}){

	const [sessionState, setSessionState] = useState("");

	function updateMedia(media, mediaID)
	{
		updateOrAddMedia(media, mediaID);
		const body = {
			"userID": mediaID,
			"databaseUserID": userID,
			"media": media
		}
		post("/updateMedia", body);
	}

	useEffect(() => {
		post("/generateState", undefined).then(jsonState => {
			post("/createBlankUser/", undefined).then(createdUserID => {
				setUserID(createdUserID);
				setSessionState(jsonState.state);
			})
		})
	}, [])

	return(

	<div id="connectParent"
			 style={{
				 display: "flex",
				 flexDirection: "column",
				 justifyContent: "center",
				 alignItems: "center",
			 }}
		>
			<Grid container rowSpacing={8} columnSpacing={6} style={{textAlign: "center"}} >
				<Grid item xs={4}>
					<SlackButton state={sessionState} userID={userID}/>
				</Grid>
				<Grid item xs={4}>
					<MessengerButton userID={userID}/>
				</Grid>
				<Grid item xs={4}>
					<TwitterButton updateMedia = {updateMedia}/>
				</Grid>

			</Grid>

	</div>
	)
}

export default Connect;

/**
 * @module AppLayout
 * @category Core
 * @description Defines the split in two website layout.
 */
import NavBar from "./NavBar";
import React from "react";
import {BackButtonContainer} from "../utilityComponents/BackButtonContainer";
import {Typography} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {hiddenStyle, showStyle} from "../AnimationStyles";


/**
 *
 * Divides the website into two big flex boxes, the navigation bar and the page's content.
 * The parameters are set through parent and children react components.
 * @param {float} navBarSize - The size of the navigation bar. Set to 0.1 to be in the middle of the screen, set to 1.
 * @param {function} setNavBarSize - The react hook state that modifies the navBarSize parameter. It is passed to children components.
 * @param {JSON} styleState - The current style applied to the page's content.
 * @param {function} setStyleState - The react hook state that modifies the styleState parameter. It is passed to children components.
 * @param {function} children - The react component the user is currently browser. This component is displayed in the lower half of the website.
 * @returns {HTMLElement}
 */
function AppLayout({ navBarSize, setNavBarSize, styleState, setStyleState, children }){
    let nav = useNavigate();
    function handleClick(path)
    {
        setStyleState(hiddenStyle);

        setTimeout(() => {
            setNavBarSize(0.1);
            nav(path);
            setTimeout(() => {
                setStyleState(showStyle)

            }, 500);
        }, 500);
    }


    return(
        <div
            id="parent div"
            style={{
                width: "100wh",
                height: "100vh",
                display: "flex",
                justifyContent: "center",
                flexDirection: "column",
                "alignItems": "center",
            }}

        >
           <div id="child div"
                style={{
                    display: "flex",
                    height: "100%",
                    width: "100%",
                    flexDirection: "column",
                    "alignContent":"center"
                }}
            >
                <BackButtonContainer setNavBarSize={setNavBarSize} setStyleState={setStyleState}/>
                <Typography  style={{
                    position: "absolute",
                    right: "1em",
                    textDecoration: "underline"
                }} variant="h6" display="inline" onClick={() => {handleClick("/about")}}>Lost? Learn about Socialbus!</Typography >
                <div id="title Flexbox" style={{ transition: "flex 800ms", flex: navBarSize}} children={<NavBar />} />
                <div style={styleState}>
                    {children}
                </div>
            </div>
        </div>
    );
}
export default AppLayout;
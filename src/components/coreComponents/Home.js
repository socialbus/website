/**
 * @module Home
 * @category Core
 * @description The default location of the website.
 */
import React, {useEffect} from "react";
import {Button, Stack, Typography} from "@mui/material";
import {useNavigate} from "react-router-dom";
import {showStyle, hiddenStyle} from "../AnimationStyles"
import Box from "@mui/material/Box";
import Welcome from "./WelcomeContent";

/**
 *
 * Displays a stack of buttons to navigate in the pages of the website.
 * @param {function} setNavBarSize - The react hook state that modifies the navBarSize parameter.
 * @param {function} setStyleState - The react hook state that modifies the styleState parameter. It is passed to children components.
 * @returns {HTMLElement}
 */
function Home({setNavBarSize, setStyleState}) {
    let navigate = useNavigate();
    /**
     *
     * When a button is clicked, Apply a different style for 500 milliseconds to apply the fade in fade out animation, then navigate to the new page.
     * @param {string} path - The path of the button clicked.
     */
    function handleClick(path)
    {
        setStyleState(hiddenStyle);

        setTimeout(() => {
            setNavBarSize(0.1);
            navigate(path);
            setTimeout(() => {
                setStyleState(showStyle)

            }, 500);
        }, 500);
    }

    useEffect(() => {
            setNavBarSize(1);
    }, [])



    return (
        <div>
            <div
                style={{
                    display: "flex",
                    flexDirection: "column",
                    height: "100%",
                    justifyContent: "flex",
                    alignItems: "center",
                    width: "100%"
                }}
            >
                <Stack direction="row" spacing={4}>
                    <Button
                        style={{
                            "width": "100px",
                        }}
                        onClick={() => {
                            handleClick("/create");
                        }}
                        type="submit"
                        color="primary"
                        variant="contained"
                    >Create</Button>
                    <Button
                        style={{
                            "width": "100px",
                        }}
                        onClick={()=>{
                            handleClick("/join");
                        }}
                        type="submit"
                        color="primary"
                        variant="contained"
                    >Join</Button>
                </Stack>
             </div>
            <div
                style={{
                    alignItems: "left"
                }}>
                <Welcome/>
            </div>
        </div>)
}

export default Home;
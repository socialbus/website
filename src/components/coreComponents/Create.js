/**
 * @module Create
 * @category Core
 * @description The page for users to create a group.
 */
import React, {useState} from "react";
import {Button, Stack, Typography} from "@mui/material";
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Connect from "./Connect";
import TextfieldWithLoading from "../utilityComponents/TextfieldWithLoading";
import ErrorDisplayer from "../utilityComponents/ErrorDisplayer";
import {verifyUserMedias} from "../../DatabaseCommunicator";
import {post} from "../../services/api/Provider";
import {NotifyGroupJoiner} from "../../services/OSNSNotifierService";
const genID = require("uuid");
/**
 * Group creation page.
 * @returns {HTMLElement}
 */
const Create = () => {

	const [medias, setMedias] =useState([]);

	const [creating, setCreating] = useState(false);
	const [created, setCreated] = useState(false);

	const [errorMessage, setErrorMessage] = useState("");
	const [displayError, setDisplayError] = useState(false);

	const [createdGroupID, setCreatedGroupID] = useState(0);
	const [userID, setUserID] = useState("");

	const [groupNameTextContent, setGroupNameTextContent] = useState("");
	const [nickNameTextContent, setNicknameTextContent] = useState("");

	const subMessageContent = "Give this key to invite other people to join the group:";

	const [createButtonText, setButtonText] = useState("Create Group");
	const [createButtonDisabled, setDisabled] = useState(false);
	/**
	 *
	 * The medias react state stores all the user media IDs. If the media in parameter already exists in the state, we update the ID, otherwise we add it.
	 * @param {string} mediaName - The media to add or update.
	 * @param {string} mediaID - The media ID to add or update.
	 */
	function updateOrAddMedia(mediaName, mediaID){

		let newMedias = medias;

		if(newMedias.find(media => media.name === mediaName) !== undefined)
		{
			const index = newMedias.findIndex(media => media.name === mediaName);
			newMedias[index].id = mediaID;
		}
		else
		{
			medias.push({
				"name":mediaName,
				"id":mediaID
			});
		}
		setMedias(newMedias);
	}

	/**
	 *
	 * Called when the create button is pressed.
	 * Does verifications that all data entered by the user is valid.
	 * If it is, displays the key of the generated group. Otherwise, displays what information is missing or wrong.
	 * @returns {Promise<void>}
	 */
	async function onCreate()
	{
		if(nickNameTextContent === "")
		{
			setCreating(false);
			setCreated(false);
			setDisplayError(true)
			setErrorMessage("Please enter a nickname to use for the group.");
			return;
		}

		const hasMedias = await verifyUserMedias(userID, medias);
		if(!hasMedias) {
			setCreating(false);
			setCreated(false);
			setDisplayError(true)
			setErrorMessage("Please connect at least one social media to the group.");
			return;
		}

		setCreating(true);
		setDisplayError(false);



		const body = {
			"groupName":groupNameTextContent,
			"id":genID.v4()
		}

		post("/create/newGroup", body).then(async function(group){
			try {
				const userData = {
					nickname:nickNameTextContent,
					currentGroupID: group.id,
					userDatabaseID: userID,
				};
				const isStored = await post("/storeUser/", { userData });
				setCreating(false);

				if(isStored)
				{
					setCreatedGroupID(group.id);
					setCreated(true)
					setButtonText("Created");
					setDisabled(true);
					NotifyGroupJoiner(medias, userID, group._id, groupNameTextContent, userData.nickname, "created");
				}
				else {
					setCreated(false);
					setDisplayError(true)
					setErrorMessage("Could not create group");
				}
			} catch (e) {
				setCreating(false);
				setCreated(false);
				setDisplayError(true)
				setErrorMessage("Could not create group");
			}
		})
	}
	return(
		<div
			style={{
				display: "flex",
				flexDirection: "column",
				justifyContent: "flex",
				alignItems: "center",
			}}
		>
			<Stack direction="column" spacing={4}>
				<Typography variant="h4" align="center">Create Group</Typography>
				<ErrorDisplayer errorMessage={errorMessage} displayError={displayError}/>

				<Box display="flex" alignItems="center"	justifyContent="center">
					<TextfieldWithLoading created={created} loading={creating} textFieldValue={groupNameTextContent} setTextFieldValue={setGroupNameTextContent} groupID={createdGroupID} mainMessage="Successfully created group." subMessage={subMessageContent} label="Group Name" displayEmail={true}/>
				</Box>

				<Box display="flex" alignItems="center"	justifyContent="center">
					<TextField id="Nickname"  label="Nickname" variant="outlined" onChange={(event
					) => setNicknameTextContent(event.target.value)} />
				</Box>
				<Connect updateOrAddMedia={updateOrAddMedia} userID={userID} setUserID={setUserID}/>
				<Box display="flex" alignItems="center"	justifyContent="center">
					<Button
						type="submit"
						color="secondary"
						variant="contained"
						disabled={createButtonDisabled}
						onClick={() => onCreate()}
					>{createButtonText}</Button>
				</Box>
			</Stack>
		</div>
	)
}

export default Create;

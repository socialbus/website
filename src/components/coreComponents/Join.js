/**
 * @module Create
 * @category Core
 * @description The page allowing for users to join a group.
 */
import React, {useEffect, useState} from "react";
import {Button, Stack, Typography} from "@mui/material";
import TextField from '@mui/material/TextField';
import Box from '@mui/material/Box';
import Connect from "./Connect";
import {verifyUserMedias} from "../../DatabaseCommunicator";
import {post} from "../../services/api/Provider";
import ErrorDisplayer from "../utilityComponents/ErrorDisplayer";
import TextfieldWithLoading from "../utilityComponents/TextfieldWithLoading";
import NotifyMembersOfJoin from "../../services/OSNSNotifierService";

/**
 * Join group page.
 * @returns {HTMLElement}
 */
const Join = () => {

	const [medias, setMedias] =useState([]);
	const [userID, setUserID] = useState("");

	const [joining, setJoining] = useState(false);
	const [joined, setJoined] = useState(false);

	const [errorMessage, setErrorMessage] = useState("");
	const [displayError, setDisplayError] = useState(false);
	const [groupID, setGroupID] = useState("");
	const [nickNameTextContent, setNicknameTextContent] = useState("");

	const [joinButtonText, setButtonText] = useState("Join Group");
	const [joinButtonDisabled, setDisabled] = useState(false);

	useEffect(() => {
		const queryParams = new URLSearchParams(window.location.search);
		const key = queryParams.get("groupKey");
		if(key == null)
			return;
		setGroupID(key);
	}, [])
	/**
	 *
	 * Called when the join button is pressed.
	 * Does verifications that all data entered by the user is valid.
	 * If it is, displays that the user successfully joined the group. Otherwise, displays what information is missing or wrong.
	 * @returns {Promise<void>}
	 */
	async function onJoin(){

		if(nickNameTextContent === "")
		{
			setJoining(false);
			setJoined(false);
			setDisplayError(true)
			setErrorMessage("Please enter a nickname to use for the group.");
			return;
		}
		const hasMedias = await verifyUserMedias(userID, medias);
		if(!hasMedias) {
			setJoining(false);
			setJoined(false);
			setDisplayError(true)
			setErrorMessage("Please connect at least one social media to the group.");
			return;
		}
		setJoining(true);
		setDisplayError(false);


		const userData = {
			nickname:nickNameTextContent,
			currentGroupID: groupID,
			userDatabaseID: userID,
		};
		const isStored = await post("/storeUser/", { userData });
		setJoining(false);

		if(isStored)
		{
			setJoined(true)
			const group = await post("/getGroupByKey", {key:groupID});
			setButtonText("Joined");
			setDisabled(true);
			NotifyMembersOfJoin(medias, userID, group._id, group.groupName, userData.nickname, "joined");
		}
		else {
			setJoined(false);
			setDisplayError(true)
			setErrorMessage("Invalid group key.");
		}
	}

	/**
	 *
	 * The medias react state stores all the user media IDs. If the media in parameter already exists in the state, we update the ID, otherwise we add it.
	 * @param {string} mediaName - The media to add or update.
	 * @param {string} mediaID - The media ID to add or update.
	 */
	function updateOrAddMedia(mediaName, mediaID){

		let newMedias = medias;

		if(newMedias.find(media => media.name === mediaName) !== undefined)
		{
			const index = newMedias.findIndex(media => media.name === mediaName);
			newMedias[index].id = mediaID;
		}
		else
		{
			medias.push({
				"name":mediaName,
				"id":mediaID
			});
		}
		setMedias(newMedias);
	}
	return(
		<div
			 style={{
				 display: "flex",
				 flexDirection: "column",
				 justifyContent: "flex",
				 alignItems: "center",
			 }}
		>
			<Stack direction="column" spacing={4}>
				<Typography variant="h4" align="center">Join Group</Typography>
				<ErrorDisplayer errorMessage={errorMessage} displayError={displayError}/>
				<Box display="flex" alignItems="center"	justifyContent="center">
					<TextfieldWithLoading created={joined} loading={joining} textFieldValue={groupID} setTextFieldValue={setGroupID} groupID="" mainMessage="Successfully joined group." subMessage="" label="Group Key" displayEmail={false}/>
				</Box>

				<Box display="flex" alignItems="center"	justifyContent="center">
					<TextField id="Nickname"  label="Nickname" variant="outlined" onChange={(event
					) => setNicknameTextContent(event.target.value)} />
				</Box>

				<Connect updateOrAddMedia={updateOrAddMedia} userID={userID} setUserID={setUserID}/>
				<Box display="flex" alignItems="center"	justifyContent="center">
				<Button
					type="submit"
					color="secondary"
					disabled={joinButtonDisabled}
					variant="contained"
					onClick={() => onJoin()}
				>{joinButtonText}</Button>
				</Box>
			</Stack>
		</div>
	)
}

export default Join;

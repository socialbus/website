import {Typography} from "@mui/material";
import React from "react";
import Box from "@mui/material/Box";

/**
 * Contains the rendered content of the about page.
 * @returns {HTMLElement}
 */
const Welcome = () => {

	return (
		<div>
			<Box sx={{ pt: 5 }}>
				<Typography variant="h4" align={"center"} >What is Socialbus?</Typography>
			</Box>

			<Box style={{
				width: "75%",
				marginLeft: "auto",
				marginRight: "auto"
			}}>
				<Typography align={"center"}>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</Typography>
			</Box>
		</div>
	)
}

export default Welcome;

import {Typography} from "@mui/material";
import React from "react";

const About = () => {

	return (
		<div
			style={{
				display: "flex",
				flexDirection: "column",
				justifyContent: "flex",
				alignItems: "center",
			}}
		>
			<Typography variant="h4">What is Socialbus?</Typography>
			<p>This page lists all available commands of socialbus with a detailed description.</p>
			<h2 id="summary">Summary</h2>
			<h3 id="-help-help-">- <a href="#help">Help</a></h3>
			<h3 id="-unrecognized-commands-unrecognized_commands-">- <a href="#unrecognized_commands">Unrecognized commands</a></h3>
			<h3 id="-connecttogroup-connect_to_group-">- <a href="#connect_to_group">ConnectToGroup</a></h3>
			<h3 id="-opensession-open_session-">- <a href="#open_session">OpenSession</a></h3>
			<h3 id="-quitsession-quit_session-">- <a href="#quit_session">QuitSession</a></h3>
			<h3 id="-getconnectedmembers-get_connected_members-">- <a href="#get_connected_members">GetConnectedMembers</a></h3>
			<h3 id="-getallmembers-get_all_members-">- <a href="#get_all_members">GetAllMembers</a></h3>
			<h3 id="-getconnectedgroups-get_connected_groups-">- <a href="#get_connected_groups">GetConnectedGroups</a></h3>
			<h3 id="-quit-a-group">- <a href="#quit_group">QuitGroup</a></h3>
			<h2 id="commands">Commands</h2>
			<h3 id="help-a-name-help-a-">help <a name="help"></a></h3>
			<p>Sends a message to the user that lists every command with a short summary of their effect.</p>
			<img src="https://notes.inria.fr/uploads/upload_33583c093eab745ad2eb49be9f24c376.png" alt=""></img>
			<p><em>Use of the help command</em></p>
			<h3 id="unrecognized-command-a-name-unrecognized_commands-a-">unrecognized command <a name="unrecognized_commands"></a></h3>
			<p>If the user types a command that does not exist, they will be suggested to type the &quot;help&quot; command to learn about available commands.</p>
			<img src="https://notes.inria.fr/uploads/upload_14164cf6b56726e3597b1e4b35bd25c6.png" alt=""></img>
			<p><em>Example of an unrecognized command</em></p>
			<h3 id="connecttogroup-a-name-connect_to_group-a-">connectToGroup <a name="connect_to_group"></a></h3>
			<p>Allows the user to receive messages from the group they connect to in real time.
				Upon connection, the user receives the last 30 messages of the conversation to have context of the current state of the discussion.</p>
			<p>If the command is not followed by a group name they have joined, they will be prompted with a list of their groups to connect to. Otherwhise, they will directly connect to the joined group. </p>
			<img src="https://notes.inria.fr/uploads/upload_9894700be2c0de20e0a996a5fbad7ebe.png" alt=""></img>
			<p><em>Example of a user connecting to a group</em></p>
			<h3 id="opensession-a-name-open_session-a-">openSession <a name="open_session"></a></h3>
			<p>When the user is connected to at least one group, the user can open a session to it. While the session is opened, every message sent to the bot afterwards are directly sent to all the members of the group.</p>
			<p>Only one session can be opened at a time.</p>
			<p>If the command is not followed by a group name they have connected to, they will be prompted with a list of the groups they are connected to. Otherwhise, they will directly open a session to the joined group. </p>
			<p>Lorsque l&#39;utilisateur est connecté à au moins un groupe, l&#39;utilisateur peut ouvrir une session. Tant que la session est ouverte, les prochains messages que l&#39;utilisateur envoie au bot sur le réseau social est envoyé à tous les autres membres du groupe.
				Qu&#39;une seul session peut être ouverte à la fois</p>
			<img src="https://notes.inria.fr/uploads/upload_16db0beed6e08913d56f6f78a15c513c.png" alt=""></img>
			<p><em>Example of a user opening a session to reply to a message</em></p>
			<h3 id="quitsession-a-name-quit_session-a-">quitSession <a name="quit_session"></a></h3>
			<p>If the user has an opened session, quits the session. Otherwhise, the bot informs them that they don&#39;t have a session opened.</p>
			<h3 id="getconnectedmembers-a-name-get_connected_members-a-">getConnectedMembers <a name="get_connected_members"></a></h3>
			<p>Lists the users currently connected to a specific group.</p>
			<h3 id="getallmembers-a-name-get_all_members-a-">getAllMembers <a name="get_all_members"></a></h3>
			<p>Lists all the users connected to the group, currently receiving messages in real time.
				Liste tous les utilisateurs du groupe.</p>
			<h3 id="getconnectedgroups-a-name-get_connected_groups-a-">getConnectedGroups<a name="get_connected_groups"></a></h3>
				<p>Lists the groups the user is currently connected to.</p>
			<h3 id="quit-group">quitGroup<a name="quit_group"></a></h3>
				<p>Lets the user quit a group. The members of the groups are notified.</p>

		</div>
	)
}

export default About;



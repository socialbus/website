/**
 * @module NavBar
 * @category Core
 */
import {Typography} from "@mui/material";
import React from "react";
import Stack from "@mui/material/Stack";
import ConditionalTypography from "../utilityComponents/ConditionalTypography";
import {useLocation} from "react-router-dom";


/**
 *
 * @returns {boolean} returns true if at the default location.
 */
function isDefaultLocation(location){
    console.log("FROM NAVBARlocation is :", location.pathname);
    return location.pathname === "/";
}
/**
 *
 * The content of the navigation bar displayed on the top of the website.
 * @returns {HTMLElement}
 * @constructor
 */

function NavBar () {
    const location = useLocation();

    return (
        <div
            style={{
                display: "flex",
                flexDirection: "column",
                height: "100%",
                justifyContent: "flex-end",
                alignItems: "center",
                width: "100%"
            }}
        >
            <div
                style={{
                    display: "flex",
                    alignItems: "stretch",
                }}>
                <Stack spacing={0.5} style={{
                    alignItems: "center"
                }}>
                    <Typography variant="h2" children="Socialbus"/>
                    <ConditionalTypography content="Group Management" sizeVariant="h4"
                                           active={isDefaultLocation(location)}/>
                    <span/>
                </Stack>

            </div>

        </div>

    );
}

export default NavBar;
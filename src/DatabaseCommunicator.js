import {post} from "./services/api/Provider";
require('dotenv').config({path:"../.env"})

async function verifyUserMedias(userID, medias){
    if(medias.length === 0)
    {
        return await post("/getUserMedias", {userID:userID}).then(databaseMedias => {
            try{
                return databaseMedias.length !== 0 && databaseMedias.length !== undefined;

            }
            catch(e)
            {
                console.log("ono");
                return false;
            }
        })
    }
    else
        return true;
}
export {verifyUserMedias};

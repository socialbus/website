require('dotenv').config({path:"../../.env"})

const clientId = process.env.REACT_APP_SLACK_CLIENT_ID;
const oidcScopes = "openid,email,profile"; // openid is required at least
const redirectUri = "https://" + process.env.REACT_APP_EXPRESS_PATH + "/slack/oauth_redirect";


function getSlackUrl(state, userID)
{
    const nonce = userID;
    console.log("link: ", `https://slack.com/openid/connect/authorize?response_type=code&state=${state}&client_id=${clientId}&scope=${oidcScopes}&redirect_uri=${redirectUri}?userID=${userID}&nonce=${nonce}`);
    return `https://slack.com/openid/connect/authorize?response_type=code&state=${state}&client_id=${clientId}&scope=${oidcScopes}&redirect_uri=${redirectUri}?userID=${userID}&nonce=${nonce}`;
}
export {getSlackUrl};

const routing = require('express');
const fetch = require('node-fetch');
const app = routing();
const databaseManager = require('./mongoose.js');
const request = require('request-promise');
const queryString = require('query-string');

const Ddos = require('ddos');
const ddos = new Ddos({burst:10, limit:15})
const mongoSanitize = require('express-mongo-sanitize');
const { WebClient } = require("@slack/web-api");

require('dotenv').config()

const jwt = require("jsonwebtoken");

const stateManager = require('../back/stateManager');
const state = new stateManager();
const client = new WebClient();

/*
* Communication from website pages, socialbus, and database are made here.
*/
async function verifyState(stateVal)
{
    console.log("verifying state");
    if (!(await state.validate(stateVal))) {
        console.error("couldn't validate state");
        return false;
    }
    console.log("valid state");
    return true;
}

app.use(routing.json());
app.use(ddos.express);
app.use(mongoSanitize());

app.post('/create/newGroup',  async function (req, res, next) {

    let body = req.body;
    console.log("req :"+ req.body.id);
    console.log("req :"+ req.body.groupName);
    const createdGroup = await databaseManager.onInsertGroup(req.body.id, req.body.groupName);

    res.send(
        {
            name:body.groupName,
            id:body.id,
            _id: createdGroup._id
        }
    );

});

app.post('/createBlankUser', async function (req,res)
{
    const user = await databaseManager.createBlankUser()
    console.log("created blank user:", user);
    res.send(user._id);

});

app.post('/getGroupByKey', async function(req, res)
{
    console.log("req body: ", req.body);
    const group = await databaseManager.findGroupByGroupKey(req.body.key);

    res.send(group);
});
app.post('/updateMedia', async function(req,res)
{
    console.log("JSON:", req.body);

    await databaseManager.updateUserMedia(req.body.databaseUserID, req.body.media, req.body.userID);
});


app.post("/generateState", async function(req,res)
{
    const stateVal = await state.generate();

    console.log("generated state :", stateVal);
    res.send({
        state:stateVal
    });
});

app.get("/verifyState", async function(req, res)
{
    console.log("routing js veirfy state with query:", req.query);
    const isValid = await verifyState(req.query.state);
    res.send({
        isValid: isValid
    });
});

app.get("/slack/oauth_redirect/", async function(req,res) {
    console.log("query:", req.query);
    if(!(await verifyState(req.query.state))) return;

    const url = 'https://' + process.env.HOSTNAME + "/express/slack/oauth_redirect?userID=" + req.query.userID; // + "?userID=" + req.query.userID;
    console.log("whole url:", url);

    const token = await client.openid.connect.token({
        client_id: process.env.SLACK_CLIENT_ID,
        client_secret: process.env.SLACK_CLIENT_SECRET,
        grant_type: "authorization_code",
        redirect_uri: url,
        code: req.query.code,
    });
    console.log(
        `openid.connect.token response: ${JSON.stringify(token, null, 2)}`
    );

    let userAccessToken = token.access_token;

    if (token.refresh_token) {
        // token.refresh_token can exist if the token rotation is enabled.
        // The following lines of code demonstrate how to refresh the token.
        // If you don't enable token rotation, you can safely remove this part.
        const refreshedToken = await client.openid.connect.token({
            client_id: process.env.SLACK_CLIENT_ID,
            client_secret: process.env.SLACK_CLIENT_SECRET,
            grant_type: "refresh_token",
            refresh_token: token.refresh_token,
        });
        console.log(
            `openid.connect.token (refresh) response: ${JSON.stringify(
                refreshedToken,
                null,
                2
            )}`
        );
        userAccessToken = refreshedToken.access_token;

    }
    // You can save the id_token (JWT string) as-is but you can decode the value this way:
    const claims = jwt.decode(token.id_token);

    const tokenWiredClient = new WebClient(userAccessToken);
    const userInfo = await tokenWiredClient.openid.connect.userInfo();

    await databaseManager.updateUserMedia(req.query.userID,"Slack", userInfo.sub)
    console.log(
        `openid.connect.userInfo response: ${JSON.stringify(userInfo, null, 2)}`
    );
    console.log(JSON.stringify(claims, null, 2));

    console.log(JSON.stringify(userInfo, null, 2));

    res.send("Thank you. You may close this tab");

});

app.post('/storeUser', async function(req,res)
{
    const userData = req.body.userData;
    console.log("userData: ", userData);
    const couldJoinGroup = await databaseManager.onInsertUser(userData.userDatabaseID, userData.currentGroupID, userData.nickname);
    console.log("could join group:", couldJoinGroup);
    res.send(couldJoinGroup);
});

app.get('/', function(req,res,next){
    console.log("default get request at url: " + req.originalUrl);
    res.send('hello world get');
});

app.post('/getUserMedias', async function (req,res)
{
    const userMedias = await databaseManager.getUserMediasByUserID(req.body.userID);
    res.send(userMedias);
})

app.get('/twitterAccessToken', function(req, res, next) {
    let url = '';
    const twitterOauth = {
        consumer_key: process.env.TWITTER_CONSUMER_KEY,
        consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
        token: process.env.TWITTER_TOKEN,
        token_secret: process.env.TWITTER_TOKEN_SECRET,
    };
    let twitterAccessTokens;

    let authRequestOptions =
        {
            url: 'https://api.twitter.com/oauth/request_token?oauth_callback=oob',
            oauth: twitterOauth
        };
    request.get(authRequestOptions).then(function (body) {
        twitterAccessTokens = queryString.parse(body);
        console.log("auth token is : ", twitterAccessTokens);

        let access_token_request_options =
            {
                url: 'https://api.twitter.com/oauth/authorize?oauth_token=' + twitterAccessTokens.oauth_token + '&force_login=true',
                oauth: twitterOauth
            };
        url = access_token_request_options.url;

    }).then(function () {
        res.send({
            url: url,
            twitterToken: twitterAccessTokens.oauth_token,
            twitterSecretToken: twitterAccessTokens.oauth_token_secret
        });
    })
})

app.post('/twitterValidatePin', function (req, res, next) {
    const twitterToken = req.body.twitterToken;
    const twitterTokenSecret = req.body.twitterTokenSecret;
    const pin = req.body.pin;

    let responseData = {};

    let access_token_request_options = {
        url: 'https://api.twitter.com/oauth/access_token?oauth_verifier=' + pin,
        oauth: {
            consumer_key: process.env.TWITTER_CONSUMER_KEY,
            consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
            token: twitterToken,
            token_secret: twitterTokenSecret
        },
    };

    console.log("twitter token: ", twitterToken);
    console.log("twitter token secret: ", twitterTokenSecret);

    request.get(access_token_request_options)
        .then(function (body) {

            let access_tokens = queryString.parse(body)
            console.log("acces tokens: ", access_tokens);
            console.log("oauth token:", access_tokens.oauth_token);
            console.log("oauth token secret:", access_tokens.oauth_token_secret);

            let subscription_request_options = {
                url: 'https://api.twitter.com/1.1/account_activity/all/' + 'Socialbus' + '/subscriptions.json',
                oauth: {
                    consumer_key: process.env.TWITTER_CONSUMER_KEY,
            	    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
            	    token: access_tokens.oauth_token,
        	    token_secret: access_tokens.oauth_token_secret
	        },
                resolveWithFullResponse: true
            }
            responseData.twitterID = access_tokens.user_id;

            return request.post(subscription_request_options)
        })
        .catch(function (err) {
            let status = err.statusCode
            console.log("status code of error is :", status);
            //console.log("ERROR JSON", err);

            if(status === 409 || status === 355)
            {
                //subscription already exists
                responseData.status = 409;
                responseData.twitterID = err.options.oauth.token.substr(0,err.options.oauth.token.indexOf('-'));
            }else if (status === 401)
            {
                responseData.status = 401
                //unauthorized aka unvalid pin
            }
        }).then(function (response) {
            if(response)
            {
                if (response.statusCode === 204) {
                    console.log("pin validated");
                    responseData.status = 204
                }
            }

            res.send(responseData);

        });
    });


app.post("/NotifyGroupMembersOfJoin", async function(req,res)
{
    console.log("req body : ", req.body);
    //The OSNS webhook it is sent to, needs to be up and running
    fetch("http://messenger:" + process.env.MESSENGERPORT + "/webhook", {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            json: true
        },
        body: JSON.stringify(req.body)
    })
});

function onGet(req, res)
{
    res.send("get");
}

app.listen(process.env.EXPRESSPORT || 3001, function () {
    console.log('express listening on port ' + process.env.EXPRESSPORT + '!');
});

app.get('/webhook',(req,res) => onGet(req,res));

module.exports = app;

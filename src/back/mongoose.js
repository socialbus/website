const mongoose = require('mongoose');
require('dotenv').config({path:"../../.env"})

mongoose.connect('mongodb://' + (process.env.MONGO_HOST || "127.0.0.1") + ':' + (process.env.MONGO_PORT || 27017) + '/website',
    {useNewUrlParser : true})

const db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error'));

function onOpen()
{
    console.log("connected to database");
}
db.once('open', () => onOpen());

let groupSchema = new mongoose.Schema({
    key: { type :String, required : true, unique : true},
    groupName: String,
    users: [
        {
            userID:mongoose.Schema.Types.ObjectId,
            nickname: String,
            connected:Boolean,
            readMessageIndex:{ type: Number, default: 0}
        }
    ]
});

let userSchema = new mongoose.Schema({
    //id:String, //Generate on creation maybe or use automatic id?
    medias: [
        {
            "media":String,
            "id": String,
	        "channel": String
        }
    ],
    groups:[{
        groupID:mongoose.Schema.Types.ObjectId,
        groupName: String,
        nickname:String
    }],
    connectedGroups : Array,
    state: String,
    target: String,
    userTarget: String

});

const user = mongoose.model('user', userSchema);
const group = mongoose.model('group', groupSchema);

function onInsertGroup(key, name)
{
    let newGroup = new group({key : key, groupName: name, users: undefined});
    newGroup.save();
    return newGroup;
}
async function createBlankUser()
{
    const newUser = new user;
    await newUser.save();
    return newUser;
}

//add medias that are in madeUser that don't exist in foundUser
async function updateNewMediasToOldUser(madeUser, foundUser)
{

    const newMedias = madeUser.medias.filter(madeUserMedia => !foundUser.medias.some(foundUserMedia => madeUserMedia.media === foundUserMedia.media && madeUserMedia.id === foundUserMedia.id));
    console.log("new medias: ", newMedias);

    foundUser.medias = foundUser.medias.concat(newMedias);

    return foundUser;

}
async function onInsertUser(userDatabaseID, groupID, nickname)
{
    console.log("userDatabaseID: ", userDatabaseID);
    console.log("groupID: ", groupID);
    console.log("nickname: ", nickname);

    //on récupère le groupe en question
    const foundGroup = await group.findOne({"key":groupID}).exec();
    //vérification de son existence
    if(!foundGroup)
    {
        console.error("could not find group");
        return false;
    }
    const madeUser = await findUserByUserID(userDatabaseID);
    if(!madeUser)
    {
        console.error("could not find user");
        return false;
    }
    console.log("made user data: ", madeUser);

    //on regarde si on connait déja l'utilisateur
    let foundUser = await user.findOne({"medias.id":{"$in":madeUser.medias.map(m => m.id)}});

    console.log("made user ID:", madeUser._id);
    if(!foundUser || foundUser._id.equals(madeUser._id))
    {
        console.log("are the same entries in database or doesn't exist");

        madeUser.groups.push({
            groupID:foundGroup._id,
            groupName: foundGroup.groupName,
            nickname:nickname
        });

        foundGroup.users.push({
            userID:madeUser._id,
            nickname: nickname,
            connected: false
        });
        await madeUser.save();
    }
    else
    {
        console.log("found user data: ", foundUser);
        console.log(" found user ID:", foundUser._id);
        foundUser = await updateNewMediasToOldUser(madeUser, foundUser);
        await deleteUserByID(madeUser._id);
        console.log("SEARCHING ID OF USER: ", foundUser._id);
        if(foundGroup.users.some(groupUser => groupUser._id.equals(foundUser._id))) {
            console.error("user already in group");
            return false;
        }
        console.log("saving group for user");

        foundUser.groups.push({
            groupID:foundGroup._id,
            groupName: foundGroup.groupName,
            nickname:nickname
        });
        foundGroup.users.push({
            userID: foundUser._id,
            nickname: nickname,
            connected: false
        });
        await foundUser.save();
    }
    await foundGroup.save();
    console.log("updating all users");
    return true;
}
const findGroupsByMediaID = (mediaID) => user.findOne({"medias.id":mediaID}).exec().then(group => group.groups);

const findGroupByGroupID = (groupID) => group.findOne({"_id":groupID}).exec();

const findGroupsbyGroupIDs = (groupIDs) => group.find({"_id":{$in : groupIDs}}).exec();

const findUserByUserID = (userID) => user.findOne({"_id":{$in : userID}}).exec();

const deleteUserByID = (userID) => user.findOneAndDelete({"_id":{$in : userID}}).exec();

const findGroupByGroupKey = (groupKey) => group.findOne({"key":groupKey}).exec();

const getUserMediasByUserID = (userID) => user.findOne({"_id":{$in : userID}}).exec().then(foundUser => {
    if(foundUser.medias) return foundUser.medias
});

async function updateUserMedia(userID, media, mediaID)
{
    let userToUpdate = await findUserByUserID(userID.toString());

    if(userToUpdate === null)
        console.error("could not update user media: Invalid UserID");
    console.log("userToUpdate", userToUpdate);
    userToUpdate.medias.push({
        "media":media,
        "id": mediaID
    });

    await userToUpdate.save();
}
async function findGroupsByGroupsID(groupsIDs)
{
    const group = await findGroupByGroupID(groupsIDs);
    console.log("found group: ", group);
    return group;
}
module.exports = {findGroupByGroupID, deleteUserByID,getUserMediasByUserID, findGroupByGroupKey, findGroupsByMediaID, findGroupsByGroupsID, onInsertGroup, onInsertUser, findGroupsbyGroupIDs, createBlankUser, findUserByUserID, updateUserMedia};

const uuid = require("uuid");

class StateManager {
    constructor() {
        this.activeStates = {};
    }
    async generate() {
        const newValue = uuid.v4();
        this.activeStates[newValue] = Date.now() + 10 * 60 * 1000; // 10 minutes
        return newValue;
    }
    async validate(state) {
        const expiresAt = this.activeStates[state];
        if (expiresAt && Date.now() <= expiresAt) {
            delete this.activeStates[state];
            return true;
        }
        return false;
    }
}

module.exports = StateManager;

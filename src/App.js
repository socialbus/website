/**
 * @module App
 * @category Core
 * @description The root of the application.
 */
import {BrowserRouter, Routes, Route} from "react-router-dom";
import Home from "./components/coreComponents/Home"
import Create from "./components/coreComponents/Create"
import Join from "./components/coreComponents/Join"
import React, {useState} from "react";
import { ThemeProvider } from "@mui/material";
import { theme } from "./theme";
import AppLayout from "./components/coreComponents/AppLayout";
import {showStyle} from "./components/AnimationStyles"
import About from "./components/coreComponents/About";

/**
 *
 * Applies the theme of the application and manages the browsing of different component pages with react-router-dom.
 * @returns {HTMLElement} The application
 */
function App() {

    const [navBarSize, setNavBarSize] = useState(0.1);

    const [styleState, setStyleState]  = useState(showStyle);

    return (
        <ThemeProvider theme={theme}>
            <BrowserRouter>
                <AppLayout navBarSize={navBarSize} setNavBarSize={setNavBarSize} styleState={styleState} setStyleState={setStyleState}>
                    {
                        <Routes>
                            {/* gives class components of pages their link*/}
                            <Route exact path="/" element={<Home setNavBarSize={setNavBarSize} setStyleState={setStyleState}/>}/>
                            <Route path="/create" element={<Create/>}/>
                            <Route path="/join" element={<Join/>}/>
                            <Route path="/about" element={<About/>}/>
                        </Routes>
                    }
                </AppLayout>
            </BrowserRouter>
        </ThemeProvider>)
}

export default App;

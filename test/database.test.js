const chai = require('chai');
const databaseManager = require('../src/back/mongoose');
const mongoose = require('mongoose');
chai.should();

function generateStaticData()
{

}
describe("Testing database user queries", function ()
{
	beforeEach(function()
	{
		for(let i = 0; i < 10; ++i)
		{
			const userData = generateStaticData();

		}
	})

	it("Should create a blankUser, add a media, find that user by mediaID and userID and delete user", async() =>
	{
		//create blank user
		const user = await databaseManager.createBlankUser();
		chai.assert.isNotNull(user)

		//find user by ID
		const userFromDatabase = await databaseManager.findUserByUserID(user._id);
		chai.expect(user._id).to.deep.equal(userFromDatabase._id);
		chai.expect(user.medias).to.deep.equal(userFromDatabase.medias);
		chai.expect(user.groups).to.deep.equal(userFromDatabase.groups);
		chai.expect(user.state).to.deep.equal(userFromDatabase.state);
		chai.expect(user.state).to.deep.equal(userFromDatabase.state);
		chai.expect(user.target).to.deep.equal(userFromDatabase.target);

		//add media to user
		const media = "Facebook";
		const mediaID = toString(Math.floor(Math.random() * Number.MAX_SAFE_INTEGER));
		await databaseManager.updateUserMedia(user._id, media, mediaID);

		//get user medias
		const userMedias = await databaseManager.getUserMediasByUserID(user._id);
		chai.expect(userMedias[0].media).to.deep.equal(media);
		chai.expect(userMedias[0].id).to.deep.equal(mediaID);

		await databaseManager.deleteUserByID(user._id);
		const nullUser = await databaseManager.findUserByUserID(user._id);

		//user should be removed
		chai.assert(nullUser == null);
	});

	it("Should fail to find a non existent user", async() =>
	{
		const id = mongoose.Types.ObjectId();
		const nullUser = await databaseManager.findUserByUserID(id);
		chai.assert(nullUser == null);
	});

});

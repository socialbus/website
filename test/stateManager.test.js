const chai = require('chai');
const stateManager = require('../src/back/stateManager')
const chaiHttp = require("chai-http");
const express = require("../src/back/routing");

chai.should();
chai.use(chaiHttp);

describe('Test stateManager', function () {
	describe('Validation', function()
	{
		it('should generate a code', async function () {
			const manager = new stateManager()
			const token = await manager.generate();
			chai.assert.isNotNull(token);
		});
		it('should validate a code ', async function () {
			const manager = new stateManager()
			const token = await manager.generate();
			const isValid = await manager.validate(token);
			chai.assert.isTrue(isValid);
		});
		it('should not validate a bogus code ', async function () {
			const manager = new stateManager()
			let isValid = await manager.validate("bogus");
			chai.assert.isNotTrue(isValid);

			isValid = await manager.validate(1234);
			chai.assert.isNotTrue(isValid);

			isValid = await manager.validate(null);
			chai.assert.isNotTrue(isValid);
		});

	})
});

describe('Test StateManager API calls', function ()
{
	it('should generate a state code', function()
	{
		chai.request(express)
			.post("/generateState")
			.end((err,response) => {
				response.should.have.status(200);
				chai.assert.isNotNull(response.body.state);
			});
	});

	it('should validate a state code', function()
	{
		chai.request(express)
			.post("/generateState")
			.end((err,response) => {
				response.should.have.status(200);
				chai.assert.isNotNull(response.body.state);
				chai.request(express)
					.get("/verifyState")
					.query({
						"state":response.body.state
					})
					.end((err,verifyRes) => {
						chai.assert.isTrue(verifyRes.body.isValid);
					})
			});
	})

	it('should not validate bogus state codes', function()
	{
		chai.request(express)
			.post("/verifyState")
			.query({
				"state":"bogus"
			})
			.end((err,response) => {
				chai.assert.isNotTrue(response.body.isValid);
			});

		chai.request(express)
			.post("/verifyState")
			.query({
				"state":1234
			})
			.end((err,response) => {
				console.log("in bogus state code the state is :", response.body);

				chai.assert.isNotTrue(response.body.isValid);
			});

		chai.request(express)
			.post("/verifyState")
			.query({
				"state":null
			})
			.end((err,response) => {

				chai.assert.isNotTrue(response.body.isValid);
			});

	})
});
const chai = require('chai');
const chaiHttp = require("chai-http");
const express = require("../src/back/routing");

chai.should();
chai.use(chaiHttp);
describe("test API calls", function () {

	it("Should return 404", (done) =>
	{
		chai.request(express)
			.get("/a/non/existant/path")
			.end((err,response) => {
				response.should.have.status(404);
				done();
			});
	});
	it("Should return 'hello world get'", (done) => {
		chai.request(express)
			.get("/")
			.end((err, response) => {
				response.should.have.status(200);
				response.text.should.be.equal("hello world get");
				done();
			});
	});
});
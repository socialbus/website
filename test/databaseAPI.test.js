const chai = require('chai');
const chaiHttp = require("chai-http");
const express = require("../src/back/routing");

chai.should();
chai.use(chaiHttp);

describe("test database API calls", function () {

	it("Should create a blankUser", (done) =>
	{
		chai.request(express)
			.post("/createBlankUser")
			.end((err,response) => {
				response.should.have.status(200);
				chai.assert.isNotNull(response.body._id);
				done();
			});
	});

	it ("Should update media of a created user", (done) =>
	{
		chai.request(express)
			.post("/createBlankUser")
			.end((err,response) => {
				response.should.have.status(200);
				chai.assert.isNotNull(response.body._id);
				chai.request(express)
					.post("/updateMedia")
				done();
			});
	});

});
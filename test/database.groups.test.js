const chai = require('chai');
const databaseManager = require('../src/back/mongoose');
const mongoose = require('mongoose');

chai.should();

describe("test database communication and queries", function(){

	let user;
	beforeEach(async function() {
		user = await databaseManager.createBlankUser();
	});

	afterEach(async function(){
		await databaseManager.deleteUserByID(user._id);
	})

	it("Should access created user by ID", async function()
	{
		const foundUser = await databaseManager.findUserByUserID(user._id);
		const foundUserID = foundUser._id.toString();
		foundUserID.should.equal(user._id.toString());
	})

	it("Should not find user by bogus ID", async function(){
		const bogusID = new mongoose.Types.ObjectId();
		const foundUser = await databaseManager.findUserByUserID(bogusID);
		bogusID.should.not.equal(user._id);
		(foundUser === null).should.be.true;
	})
})